const argv = require('optimist')
    .alias('h', 'hide-actions')
    .boolean('h')
    .argv;

const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const initMenu = require('./menu')

const path = require('path')
const url = require('url')

let menu = initMenu(app, createWindow);

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let scxmlWindows = {};

function createWindow (scxmlFile) {

  if(scxmlWindows[scxmlFile]) return;   //prevent duplicate windows

  app.addRecentDocument(scxmlFile);

  // Create the browser window.
  let window = new BrowserWindow({
    title: 'scxml://' + scxmlFile
  })

  // and load the index.html of the app.
  window.loadURL(`file://${__dirname}/index.html?scxmlFile=${scxmlFile}&hideActions=${argv["hide-actions"]}`)

  // Open the DevTools.
  //window.webContents.openDevTools()

  // Emitted when the window is closed.
  window.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    scxmlWindows[scxmlFile] = null;
    if(Object.keys(scxmlWindows).every(k => scxmlWindows[k] === null)) app.quit()    //close app if all windows are closed.
  })

  scxmlWindows[scxmlFile] = window;
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function(){
  const scxmlFilesToOpen = argv._; 
  
  scxmlFilesToOpen.
    filter( (file) => !scxmlWindows[file] ).
    forEach( createWindow ); 
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

